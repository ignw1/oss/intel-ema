# PoC Examples for Intel Endpoint Management Assistant (EMA)

Includes example **Terraform** deployment of EMA infrastructure to **Azure**, and example **PowerShell** REST scripts for manipulating EMA/endpoints.

## About the Terraform deployment to Azure

The included Terraform deployment implements the manual deployment described in the document "Intel Endpoint Management Assistant: Deployment Guide for Microsoft Azure" v1.7, which is available to [download from Intel](https://www.intel.com/content/www/us/en/support/articles/000058623/software/manageability-products.html).

This Terraform can be deployed in its entirity for PoCs, or you can cherry pick the key modules to use as a starting point for integrating the EMA infrastructure into your existing Terraform structure.

After deploying your Azure infrastructure with Terraform, you'll still need to install the EMA software, as described in the document "Intel Endpoint Management Assistant Distributed Server Installation and Maintenance Guide." This guide is also available to [download from Intel](https://www.intel.com/content/www/us/en/support/articles/000058623/software/manageability-products.html).
