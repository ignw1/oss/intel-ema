#Requires -Version 5
# Example script provided by Intel
param (
    [Parameter(Mandatory = $false, Position=0)]
    [string]$action, 
    [Parameter(Mandatory = $false, Position=1)]
    [string]$epg,
    [Parameter(Mandatory = $false, Position=2)]
    [string]$alarmname,
    [Parameter(Mandatory = $false, Position=3)]
    [string]$alarmtime,
    [Parameter(Mandatory = $false, Position=4)]
    [string]$alarminter,
    [Parameter(Mandatory = $false, Position=5)]
    [string]$server, 
    [Parameter(Mandatory = $false, Position=6)]
    [string]$user, 
    [Parameter(Mandatory = $false, Position=7)]
    [string]$password,
    [Parameter(Mandatory = $false, Position=8)]
    [switch]$ad,
    [Parameter(Mandatory = $false, Position=9)]
    [switch]$na,
    [Parameter(Mandatory = $false, Position=10)]
    [switch]$noverify
)
# Function definition section
#
# Sequential or designated token creation for Active Directory or normal accounts
function Get-AuthToken {
    $global:authenticated = $false
    Write-Host -Foreground Cyan "Server: $emasrv User: $user Password: ***********"
    $body = @{
        "Password" = $password
        "Upn" = $user
    }
    $json = $body | ConvertTo-Json
    If ($ad) {
        try {
            $token = (Invoke-RestMethod -Uri https://$emasrv/api/latest/accessTokens/getUsingWindowsCredentials -ContentType 'application/json' -Method POST -Body $json) | Select-Object -Expand access_token
            $global:headers = @{'Authorization' = 'Bearer ' + $token}
            Write-Host -Foreground Green "Authentication token for server $emasrv using account $user created successfully."
            $global:authenticated = $true
        } catch {Write-Host -Foreground Yellow "Unable to authenticate using Active Directory credentials." $_}
    } ElseIf ($na) {
        try {
            $token = (Invoke-RestMethod -Uri https://$emasrv/api/token -Method POST -Body "grant_type=password&username=$user&password=$password") | Select-Object -Expand access_token
            $global:headers = @{'Authorization' = 'Bearer ' + $token}
            Write-Host -Foreground Green "Authentication token for server $emasrv using account $user created successfully."
            $global:authenticated = $true
        } catch {Write-Host -Foreground Yellow "Unable to authenticate using normal account credentials." $_}
    } Else {
        try {
            $token = (Invoke-RestMethod -Uri https://$emasrv/api/latest/accessTokens/getUsingWindowsCredentials -ContentType 'application/json' -Method POST -Body $json) | Select-Object -Expand access_token
            $global:headers = @{'Authorization' = 'Bearer ' + $token}
            Write-Host -Foreground Green "Authentication token for server $emasrv using account $user created successfully."
            $global:authenticated = $true
        } catch {Write-Host -Foreground Yellow "Unable to authenticate using Active Directory credentials. $_ `nAttempting to use normal account credentials...`n"}
    If ($authenticated -eq $true) {Return}
        try {
            $token = (Invoke-RestMethod -Uri https://$emasrv/api/token -Method POST -Body "grant_type=password&username=$user&password=$password") | Select-Object -Expand access_token
            $global:headers = @{'Authorization' = 'Bearer ' + $token}
            Write-Host -Foreground Green "Authentication token for server $emasrv using account $user created successfully."
            $global:authenticated = $true
        } catch {Write-Host -Foreground Yellow "Unable to authenticate using normal account credentials." $_}
        If ($authenticated -eq $true) {
            Return
        } else {
            Write-Host -Foreground Yellow "`nUnable to authenticate using either type of credential."
            exit 1
        }
    }
}

# Gather IDs
function Get-IDs {
    $authenticate = $null
    If ([string]::IsNullOrEmpty($user)) {
        $authenticate = $false
    } Else {
        $authenticate = $true
    }
    If ($authenticate) {
        Get-AuthToken
    }
    Try {
        $script:groupId = ((Invoke-RestMethod -Uri https://$emasrv/api/latest/endpointGroups -ContentType 'application/json' -Method GET -Headers $headers) | Where-Object -Property Name -eq $epg).EndpointGroupId
        $script:endpointIds = ((Invoke-RestMethod -Uri https://$emasrv/api/latest/endpoints/?endpointGroupId=$groupId -Method GET -Headers $headers).EndpointId)
        $script:endpointNames = ((Invoke-RestMethod -Uri https://$emasrv/api/latest/endpoints/?endpointGroupId=$groupId -Method GET -Headers $headers).ComputerName)
        Write-Host -ForegroundColor Green "Successfully gathered Endpoint Group ID and Endpoint IDs for: $epg"
    } Catch {
        Write-Host -ForegroundColor Yellow "Failed to gather Endpoint Group ID for: $epg"
        Return
    }
}

# Set the hardware alarm clock for a given Endpoint Group
function Set-AlarmClocks {
    $alarminter = $alarminter.Split(' ')
    $days = $alarminter[0]
    $hours = $alarminter[1]
    $minutes = $alarminter[2]
    $body = @{
        "Name" = $alarmname
        "StartTime" = $alarmtime
        "Interval" = @{
            "Days" = $days
            "Hours" = $hours
            "Minutes" = $minutes
        }
    "DeleteOnCompletion" = $false
    }
    $json = $body | ConvertTo-Json
    Get-IDs
    [int]$x = "-1"
    foreach ($_ in $endpointIds) {
        $x++
        $outputId = $endpointIds[$x]
        $outputName = $endpointNames[$x]
        Write-Host -ForegroundColor Green "Endpoint $x ID: $outputId"
        Try {
            (Invoke-RestMethod -Uri https://$emasrv/api/latest/endpointOOBOperations/Single/AlarmClock/$outputId -Method POST -ContentType 'application/json' -Body $json -Headers $headers) | Out-Null
            Write-Host -ForegroundColor Green "Alarm set for: $outputName"
        } Catch {
            Write-Host -ForegroundColor Yellow "Unable to set an alarm for: $outputName`nEndpoint might be offline."
        }
        $outputId = $null
        $outputName = $null
    }
}

# Deletes all hardware alarm clock for a given Endpoint Group
function Delete-AlarmClocks {
    $body = @{
        "Name" = $alarmname
    }
    $json = $body | ConvertTo-Json
    Get-IDs
    [int]$x = "-1"
    foreach ($_ in $endpointIds) {
        $x++
        $outputId = $endpointIds[$x]
        $outputName = $endpointNames[$x]
        Write-Host -ForegroundColor Green "Endpoint $x ID: $outputId"
        Try {
            (Invoke-RestMethod -Uri https://$emasrv/api/latest/endpointOOBOperations/Single/AlarmClock/$outputId -Method DELETE -ContentType 'application/json' -Body $json -Headers $headers) | Out-Null
            Write-Host -ForegroundColor Green "Alarm deleted for: $outputName"
        } Catch {
            Write-Host -ForegroundColor Yellow "Unable to delete an alarm for: $outputName`nEndpoint might be offline."
        }
        $outputId = $null
        $outputName = $null
    }
}

# Wakes endpoints in a given Endpoint Group by iterating through each ID
function Get-SingleFileEPWake {
    Get-IDs
    [int]$x = "-1"
    foreach ($_ in $endpointIds) {
        $x++
        $outputId = $endpointIds[$x]
        $outputName = $endpointNames[$x]
        Write-Host -ForegroundColor Green "Endpoint $x ID: $outputId"
        $body = @{
            "EndpointId" = $outputId
        }
        $json = $body | ConvertTo-Json
        Try {
            (Invoke-RestMethod -Uri https://$emasrv/api/latest/endpointOOBOperations/Single/PowerOn -Method POST -ContentType 'application/json' -Body $json -Headers $headers) | Out-Null
            Write-Host -ForegroundColor Green "Sent WAKE to: $outputName"
        } Catch {
            Write-Host -ForegroundColor Yellow "Unable to send WAKE to: $outputName`nEndpoint might be offline."
        }
        $outputId = $null
        $outputName = $null
    }
}

# Wakes endpoints in a given Endpoint Group using the batch API
function Get-BulkEPWake {
    Get-IDs
    [int]$x = "-1"
    foreach ($_ in $endpointIds) {
        $x++
        $endpointIds[$x] = '{"EndpointId": "'+$endpointIds[$x]+'"},'+"`n"
    }
    [int]$last = ($endpointIds.Count - 1)
    $endpointIds[$last] = $endpointIds[$last].Replace(',','')
    $endpointIds[0] = " "+$endpointIds[0]
    $json = "[`n"+$endpointIds+"]"
    Try {
        (Invoke-WebRequest -Uri https://$emasrv/api/latest/endpointOOBOperations/Multiple/PowerOn -Method POST -ContentType 'application/json' -Body $json -Headers $headers) | Out-Null
        Write-Host -ForegroundColor Green "Sent WAKE to all endpoints in: $epg"
    } Catch {
        Write-Host -ForegroundColor Yellow "Unable to send WAKE to: $epg"
    }
}

# Main execution section
#
# Set certificate handling for < Windows 11 first if -noverify option set to true
$psver = ($PSVersionTable.PSVersion).ToString()
If ($noverify -And $psver -lt 6) {
add-type @"
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    public class TrustAllCertsPolicy : ICertificatePolicy {
        public bool CheckValidationResult(
            ServicePoint srvPoint, X509Certificate certificate,
            WebRequest request, int certificateProblem) {
            return true;
        }
    }
"@
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Ssl3, [Net.SecurityProtocolType]::Tls, [Net.SecurityProtocolType]::Tls11, [Net.SecurityProtocolType]::Tls12
} ElseIf ($noverify -And $psver -ge 6) {
    $PSDefaultParameterValues["Invoke-RestMethod:SkipCertificateCheck"]=$true
}
# Set global variables
$global:emasrv = $server

# Set default output text color to cyan
$global:defColor = $Host.UI.RawUI.ForegroundColor
$Host.UI.RawUI.ForegroundColor = "Cyan"

# Action parsing section
# Un-comment lines below for script to parse CLI actions
If ($action -eq "Get-AuthToken") {
    Get-AuthToken
} ElseIf ($action -eq "Set-AlarmClocks") {
    Set-AlarmClocks
} ElseIf ($action -eq "Delete-AlarmClocks") {
    Delete-AlarmClocks
} ElseIf ($action -eq "Get-SingleFileEPWake") {
    Get-SingleFileEPWake
} ElseIf ($action -eq "Get-BulkEPWake") {
    Get-BulkEPWake
} Else {
 cls
 Write-Host -ForegroundColor Yellow "Must specify one action."
 Write-Host -ForegroundColor Magenta "Available actions:"
 Write-Host -ForegroundColor Cyan " Get-AuthToken			Get an authentication token by attempting AD auth and then normal account auth
 Set-AlarmClocks		Sets the hardware alarm clock for all endpoints in a group
 Get-SingleFileEPWake		Iterates through all endpoints in a group and sends WAKE
 Get-BulkEPWake			Sends batch WAKE to all endpoints in a group"
 Write-Host -ForegroundColor Magenta "Switches:"
 Write-Host -ForegroundColor Green " -server			Defines the Intel(R) EMA server

 -user				Defines the user account for the Intel(R) EMA server

 -password			Defines the user password for the Intel(R) EMA server

 -epg				Defines the endpoint group to target for action

 -ad				Optional, uses Active Directory Authentication for Intel(R) EMA server

 -na				Optional, uses Normal Account Authentication for Intel(R) EMA server

 -noverify			Optional, bypasses certificate checking for use with non-trusted root certificates

 -alarmname			Required for Set-AlarmClocks, defines the name of the alarm to create

 -alarmtime			Required for Set-AlarmClocks, time of the alarm, ""yyyy-MM-ddTHH:mm:ssZ"" UTC format

 -alarminter			Required for Set-AlarmClocks, defines interval for alarm, ""DAYS HOURS MINUTES"" format"
 Write-Host -ForegroundColor Magenta "Examples:"
 Write-Host -ForegroundColor White -NoNewLine " AMT-AlarmsandWake.ps1";Write-Host -ForegroundColor Cyan -NoNewLine " Set-AlarmClocks";Write-Host -ForegroundColor Green " -server `'ema.domain.com`' -user `'tenantadmin@domain.com`' -password `'StrongPass1!`' -epg `'ACM-Group`' -alarmname `'Default`' -alarmtime `'2022-01-01T00:00:00Z`' -alarminter `'30 0 0`'"
 Write-Host -ForegroundColor White -NoNewLine "`n AMT-AlarmsandWake.ps1";Write-Host -ForegroundColor Cyan -NoNewLine " Get-SingleFileEPWake";Write-Host -ForegroundColor Green " -server `'ema.domain.com`' -user `'tenantadmin@domain.com`' -password `'StrongPass1!`' -epg `'ACM-Group`'"
 Write-Host -ForegroundColor White -NoNewLine "`n AMT-AlarmsandWake.ps1";Write-Host -ForegroundColor Cyan -NoNewLine " Get-BulkEPWake";Write-Host -ForegroundColor Green " -server `'ema.domain.com`' -user `'tenantadmin@domain.com`' -password `'StrongPass1!`' -epg `'ACM-Group`'"
}

# Reset default output text color
$Host.UI.RawUI.ForegroundColor = $defColor
$global:defColor = $null

