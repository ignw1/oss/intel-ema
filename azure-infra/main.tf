module "resource_group" {
  source   = "./modules/resource_group"
  name     = var.resource_group_name
  location = var.location
  tags     = local.common_tags
}

module "vnet" {
  source              = "./modules/vnet"
  location            = var.location
  resource_group_name = module.resource_group.name
  tags                = local.common_tags
}

module "azure_bastion" {
  source              = "./modules/azure_bastion"
  vnet_name           = module.vnet.name
  location            = var.location
  resource_group_name = module.resource_group.name
  tags                = local.common_tags
}

module "ema_servers_subnet" {
  source                          = "./modules/ema_servers_subnet"
  vnet_name                       = module.vnet.name
  bastion_subnet_addresses        = module.azure_bastion.bastion_subnet_address_prefixes
  trusted_web_ui_source_addresses = var.trusted_web_ui_source_addresses
  location                        = var.location
  resource_group_name             = module.resource_group.name
  tags                            = local.common_tags
}

module "load_balancer" {
  source              = "./modules/load_balancer"
  location            = var.location
  resource_group_name = module.resource_group.name
  tags                = local.common_tags
}

module "ema_server_vms" {
  source                     = "./modules/ema_server_vms"
  ema_servers_subnet_id      = module.ema_servers_subnet.ema_servers_subnet_id
  windows_admin_username     = var.ema_servers_admin_username
  windows_admin_password     = var.ema_servers_admin_password
  lb_backend_address_pool_id = module.load_balancer.backend_address_pool_id
  location                   = var.location
  resource_group_name        = module.resource_group.name
  tags                       = local.common_tags
}

module "sql_server" {
  source                = "./modules/sql_server"
  sql_admin_username    = var.sql_admin_username
  sql_admin_password    = var.sql_admin_password
  ema_servers_subnet_id = module.ema_servers_subnet.ema_servers_subnet_id
  location              = var.location
  resource_group_name   = module.resource_group.name
  tags                  = local.common_tags
}
