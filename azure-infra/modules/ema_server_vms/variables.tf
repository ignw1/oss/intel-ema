variable "ema_server_vm_name_prefix" {
  type        = string
  description = "Name prefix for the EMA Server VMs. A number suffix will be appended to create the full VM name"
  default     = "ema-server"
}

variable "ema_servers_avail_name" {
  type        = string
  description = "Name of the Availability Set for the EMA server VMs"
  default     = "ema-servers"
}

variable "ema_servers_subnet_id" {
  type        = string
  description = "ID of the subnet for the EMA servers"
}

variable "windows_admin_username" {
  type        = string
  description = "Windows Admin username for the EMA server VMs"
  sensitive   = true
}

variable "windows_admin_password" {
  type        = string
  description = "Windows Admin password for the EMA server VMs"
  sensitive   = true
}

variable "lb_backend_address_pool_id" {
  type        = string
  description = "ID of the load balancer's backend address pool, to be associated with the VMs' network interfaces"
}

variable "location" {
  type        = string
  description = "Region where the Intel EMA resources will reside"
}

variable "resource_group_name" {
  type        = string
  description = "Name of the Resource Group in which to deploy the Intel EMA resources"
}

variable "tags" {
  type        = map(string)
  description = "Any tags that should added to the EMA resources"
}
