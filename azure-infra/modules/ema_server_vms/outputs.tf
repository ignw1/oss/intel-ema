output "ema_server_vm_count" {
  description = "The number of EMA server VMs"
  value       = local.ema_server_vm_count
}

output "ema_servers_nic_ids" {
  description = "List of each EMA server's network interface ID"
  value       = azurerm_network_interface.ema_server[*].id
}

output "ema_servers_nic_mac_addresses" {
  description = "List of each EMA server's MAC address"
  value       = azurerm_network_interface.ema_server[*].mac_address
}

output "ema_servers_nic_private_ips" {
  description = "List of each EMA server's private IP address"
  value       = azurerm_network_interface.ema_server[*].private_ip_address
}

output "ema_servers_nic_dns_suffix" {
  description = "List of each EMA server's DNS domain suffix"
  value       = azurerm_network_interface.ema_server[*].internal_domain_name_suffix
}

output "ema_servers_vm_ids" {
  description = "List of each EMA server's VM ID"
  value       = azurerm_windows_virtual_machine.ema_server[*].id
}

output "ema_servers_vm_names" {
  description = "List of each EMA server's VM name"
  value       = azurerm_windows_virtual_machine.ema_server[*].name
}

output "ema_servers_identities" {
  description = "List of each EMA server's identity map, which includes principal_id and tenant_id"
  value       = azurerm_windows_virtual_machine.ema_server[*].identity
}

output "ema_servers_vm_ids_128bit" {
  description = "List of each EMA server's 128-bit VM ID"
  value       = azurerm_windows_virtual_machine.ema_server[*].virtual_machine_id
}

output "ema_servers_lb_pool_association_ids" {
  description = "List of each EMA server's load balancer backend pool association"
  value       = azurerm_network_interface_backend_address_pool_association.ema_server[*].id
}
