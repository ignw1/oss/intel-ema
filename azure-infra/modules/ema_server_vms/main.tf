# Set the number of duplicate EMA server VMs to create = 2
locals {
  ema_server_vm_count = 2
}

# Network Interface
resource "azurerm_network_interface" "ema_server" {
  count               = local.ema_server_vm_count
  name                = "${var.ema_server_vm_name_prefix}-${count.index}"
  location            = var.location
  resource_group_name = var.resource_group_name

  ip_configuration {
    name                          = "private-ip1"
    subnet_id                     = var.ema_servers_subnet_id
    private_ip_address_allocation = "Dynamic"
  }
}

# Availability Set
resource "azurerm_availability_set" "ema_servers" {
  name                = var.ema_servers_avail_name
  location            = var.location
  resource_group_name = var.resource_group_name
  tags                = var.tags
}

# Windows Server 2019 VM
resource "azurerm_windows_virtual_machine" "ema_server" {
  count               = local.ema_server_vm_count
  name                = "${var.ema_server_vm_name_prefix}-${count.index}"
  resource_group_name = var.resource_group_name
  location            = var.location
  size                = "Standard_D4s_v3" # Standard_E2sv3 was recommended in EMA docs but is no longer available
  admin_username      = var.windows_admin_username
  admin_password      = var.windows_admin_password
  availability_set_id = azurerm_availability_set.ema_servers.id

  network_interface_ids = [azurerm_network_interface.ema_server[count.index].id]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Premium_LRS"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2019-datacenter-gensecond"
    version   = "latest"
  }
}

# Additional disk for application logs
resource "azurerm_managed_disk" "logs" {
  count                = local.ema_server_vm_count
  name                 = "${var.ema_server_vm_name_prefix}-${count.index}_logs"
  location             = var.location
  resource_group_name  = var.resource_group_name
  storage_account_type = "Standard_LRS"
  create_option        = "Empty"
  disk_size_gb         = 256
}

# Attach application log disk to VM
resource "azurerm_virtual_machine_data_disk_attachment" "example" {
  count              = local.ema_server_vm_count
  managed_disk_id    = azurerm_managed_disk.logs[count.index].id
  virtual_machine_id = azurerm_windows_virtual_machine.ema_server[count.index].id
  lun                = "0"
  caching            = "ReadOnly"
}

# Associate the Network Interface with the Load Balancer
resource "azurerm_network_interface_backend_address_pool_association" "ema_server" {
  count                   = local.ema_server_vm_count
  network_interface_id    = azurerm_network_interface.ema_server[count.index].id
  ip_configuration_name   = azurerm_network_interface.ema_server[count.index].ip_configuration[0].name
  backend_address_pool_id = var.lb_backend_address_pool_id
}
