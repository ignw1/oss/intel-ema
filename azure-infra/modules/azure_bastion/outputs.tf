output "bastion_subnet_id" {
  description = "Subnet ID of the bastion subnet"
  value       = azurerm_subnet.bastion_service.id
}

output "bastion_subnet_name" {
  description = "Name of the bastion subnet"
  value       = azurerm_subnet.bastion_service.name
}

output "bastion_subnet_address_prefixes" {
  description = "List of the bastion subnet's address prefixes"
  value       = azurerm_subnet.bastion_service.address_prefixes
}

output "bastion_host_pip_id" {
  description = "ID of the bastion host's Public IP address"
  value       = azurerm_public_ip.bastion_service.id
}

output "bastion_host_pip_name" {
  description = "Name of the bastion host's Public IP address"
  value       = azurerm_public_ip.bastion_service.name
}

output "bastion_host_pip_address" {
  description = "Public IP address of the bastion host"
  value       = azurerm_public_ip.bastion_service.ip_address
}

output "bastion_host_id" {
  description = "ID of the bastion host"
  value       = azurerm_bastion_host.this.id
}

output "bastion_host_fqdn" {
  description = "FQDN of the bastion host"
  value       = azurerm_bastion_host.this.dns_name
}
