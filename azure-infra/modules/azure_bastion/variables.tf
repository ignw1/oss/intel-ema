variable "vnet_name" {
  type        = string
  description = "Name of the Virtual Network for Intel EMA resources"
}

/*
variable "bastion_service_subnet_name" {
  type        = string
  description = "Name of the subnet for the Azure Bastion Service"
  default     = "bastion"
}
*/

variable "bastion_service_subnet_address_prefixes" {
  type        = list(string)
  description = "Subnet address range for the Azure Bastion Service"
  default     = ["10.250.0.64/26"]
}

variable "bastion_service_pip_name" {
  type        = string
  description = "Name of the Public IP for the Azure Bastion Service"
  default     = "bastion"
}

variable "bastion_host_name" {
  type        = string
  description = "Name of the Azure Bastion Host"
  default     = "bastion"
}

variable "location" {
  type        = string
  description = "Region where the Intel EMA resources will reside"
}

variable "resource_group_name" {
  type        = string
  description = "Name of the Resource Group in which to deploy the Intel EMA resources"
}

variable "tags" {
  type        = map(string)
  description = "Any tags that should added to the EMA resources"
}
