# Note: we don't need to bother with NSGs for the Azure Bastion subnet
# Reference: https://docs.microsoft.com/en-us/azure/bastion/bastion-overview#key

# Bastion Subnet
resource "azurerm_subnet" "bastion_service" {
  # Name must be exactly 'AzureBastionSubnet' to be used for the Azure Bastion Host resource
  name                 = "AzureBastionSubnet"
  resource_group_name  = var.resource_group_name
  virtual_network_name = var.vnet_name
  address_prefixes     = var.bastion_service_subnet_address_prefixes
}

# Public IP
resource "azurerm_public_ip" "bastion_service" {
  name                = var.bastion_service_pip_name
  location            = var.location
  resource_group_name = var.resource_group_name
  allocation_method   = "Static"
  sku                 = "Standard"
  tags                = var.tags
}

# Azure Bastion Service Host
resource "azurerm_bastion_host" "this" {
  name                = var.bastion_host_name
  location            = var.location
  resource_group_name = var.resource_group_name

  ip_configuration {
    name                 = "configuration"
    subnet_id            = azurerm_subnet.bastion_service.id
    public_ip_address_id = azurerm_public_ip.bastion_service.id
  }

  tags = var.tags
}
