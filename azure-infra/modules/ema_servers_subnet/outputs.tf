output "ema_servers_subnet_id" {
  description = "Subnet ID of the EMA servers' subnet"
  value       = azurerm_subnet.ema_servers.id
}

output "ema_servers_subnet_name" {
  description = "Name of the EMA servers' subnet"
  value       = azurerm_subnet.ema_servers.name
}

output "ema_servers_subnet_address_prefixes" {
  description = "List of the EMA servers' subnet's address prefixes"
  value       = azurerm_subnet.ema_servers.address_prefixes
}

output "ema_servers_asg_id" {
  description = "ID of the EMA servers' Application Security Group"
  value       = azurerm_application_security_group.ema_servers.id
}

output "ema_servers_asg_name" {
  description = "Name of the EMA servers' Application Security Group"
  value       = azurerm_application_security_group.ema_servers.name
}

output "ema_servers_nsg_id" {
  description = "ID of the EMA server subnet's Network Security Group"
  value       = azurerm_network_security_group.ema_servers.id
}

output "ema_servers_nsg_name" {
  description = "Name of the EMA server subnet's Application Security Group"
  value       = azurerm_network_security_group.ema_servers.name
}
