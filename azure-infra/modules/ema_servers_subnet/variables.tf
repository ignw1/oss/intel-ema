variable "vnet_name" {
  type        = string
  description = "Name of the Virtual Network for Intel EMA resources"
  default     = "intel-ema"
}

variable "ema_servers_subnet_name" {
  type        = string
  description = "Name of of the subnet for the EMA server VMs"
  default     = "ema-servers"
}

variable "ema_servers_subnet_address_prefixes" {
  type        = list(string)
  description = "List of address ranges for the EMA server VMs"
  default     = ["10.250.0.0/26"]
}

variable "ema_servers_asg_name" {
  type        = string
  description = "Name of the EMA servers' Application Security Group"
  default     = "ema-servers"
}

variable "ema_servers_subnet_nsg_name" {
  type        = string
  description = "Name of the Network Security Group for the EMA servers' subnet"
  default     = "intel-ema"
}

variable "ema_endpoints_addresses" {
  type        = list(string)
  description = "List of address ranges from which the endpoints can communicate with the EMA servers. Use [\"0.0.0.0/0\"] for remote/Internet endpoints."
  default     = ["0.0.0.0/0"]
}

variable "bastion_subnet_addresses" {
  type        = list(string)
  description = "List of address ranges in the bastion subnet"
}

variable "trusted_web_ui_source_addresses" {
  type        = list(string)
  description = "List of address ranges from which trusted EMA users can access the Web UI. Only use [\"0.0.0.0/0\"] for brief testing, and with caution."
}

variable "location" {
  type        = string
  description = "Region where the Intel EMA resources will reside"
}

variable "resource_group_name" {
  type        = string
  description = "Name of the Resource Group in which to deploy the Intel EMA resources"
}

variable "tags" {
  type        = map(string)
  description = "Any tags that should added to the EMA resources"
}
