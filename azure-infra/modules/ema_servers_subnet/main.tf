# Subnet for EMA Servers
resource "azurerm_subnet" "ema_servers" {
  name                 = var.ema_servers_subnet_name
  resource_group_name  = var.resource_group_name
  virtual_network_name = var.vnet_name
  address_prefixes     = var.ema_servers_subnet_address_prefixes
  service_endpoints    = ["Microsoft.Sql"] # Required for EMA servers to connect to Azure AQL Server
}

# Application Security Group (ASG) for EMA Servers
resource "azurerm_application_security_group" "ema_servers" {
  name                = var.ema_servers_asg_name
  location            = var.location
  resource_group_name = var.resource_group_name
  tags                = var.tags
}

# Network Security Group (NSG) for EMA Servers' subnet
resource "azurerm_network_security_group" "ema_servers" {
  name                = var.ema_servers_subnet_nsg_name
  location            = var.location
  resource_group_name = var.resource_group_name

  security_rule {
    name                                       = "RDP"
    priority                                   = 100
    direction                                  = "Inbound"
    access                                     = "Allow"
    protocol                                   = "Tcp"
    source_port_range                          = "*"
    destination_port_range                     = "3389"
    source_address_prefixes                    = var.bastion_subnet_addresses
    destination_application_security_group_ids = [azurerm_application_security_group.ema_servers.id]
  }
  security_rule {
    name                                       = "WebUI"
    description                                = "Web interface for EMA users and REST scripts"
    priority                                   = 110
    direction                                  = "Inbound"
    access                                     = "Allow"
    protocol                                   = "Tcp"
    source_port_range                          = "*"
    destination_port_ranges                    = ["443", "8084"]
    source_address_prefixes                    = var.trusted_web_ui_source_addresses
    destination_application_security_group_ids = [azurerm_application_security_group.ema_servers.id]
  }
  security_rule {
    name                                       = "Swarm"
    description                                = "Connections to the EMA (Swarm) Servers from their endpoint laptops/PCs"
    priority                                   = 120
    direction                                  = "Inbound"
    access                                     = "Allow"
    protocol                                   = "Tcp"
    source_port_range                          = "*"
    destination_port_range                     = "8080"
    source_address_prefixes                    = var.ema_endpoints_addresses
    destination_application_security_group_ids = [azurerm_application_security_group.ema_servers.id]
  }
  security_rule {
    name                                       = "EMAInternal"
    description                                = "Comminication between the EMA (Swarm) Servers"
    priority                                   = 130
    direction                                  = "Inbound"
    access                                     = "Allow"
    protocol                                   = "Tcp"
    source_port_range                          = "*"
    destination_port_ranges                    = ["8092-8094", "8089"]
    source_application_security_group_ids      = [azurerm_application_security_group.ema_servers.id]
    destination_application_security_group_ids = [azurerm_application_security_group.ema_servers.id]
  }

}

# Associate NSG and Subnet
resource "azurerm_subnet_network_security_group_association" "ema_servers" {
  subnet_id                 = azurerm_subnet.ema_servers.id
  network_security_group_id = azurerm_network_security_group.ema_servers.id
}
