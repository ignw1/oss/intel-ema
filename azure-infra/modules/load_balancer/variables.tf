variable "load_balancer_name" {
  type        = string
  description = "Name of the Load Balancer"
  default     = "ema-servers"
}

variable "web_ui_pip_name" {
  type        = string
  description = "Name of the Public IP for Web UI traffic"
  default     = "web-ui"
}

variable "swarm_pip_name" {
  type        = string
  description = "Name of the Public IP for endpoint PCs/laptops to connect to the EMA swarm component"
  default     = "swarm"
}

variable "location" {
  type        = string
  description = "Region where the Intel EMA resources will reside"
}

variable "resource_group_name" {
  type        = string
  description = "Name of the Resource Group in which to deploy the Intel EMA resources"
}

variable "tags" {
  type        = map(string)
  description = "Any tags that should added to the EMA resources"
}
