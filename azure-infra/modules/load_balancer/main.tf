# Public IP Address for the EMA Web UI
resource "azurerm_public_ip" "web_ui" {
  name                = var.web_ui_pip_name
  resource_group_name = var.resource_group_name
  location            = var.location
  allocation_method   = "Static"
  sku                 = "Standard"
  tags                = var.tags
}

# Public IP Address for endpoints to contact EMA's swarm component
resource "azurerm_public_ip" "swarm" {
  name                = var.swarm_pip_name
  resource_group_name = var.resource_group_name
  location            = var.location
  allocation_method   = "Static"
  sku                 = "Standard"
  tags                = var.tags
}

# Azure Standard Load Balancer
resource "azurerm_lb" "this" {
  name                = var.load_balancer_name
  resource_group_name = var.resource_group_name
  location            = var.location
  sku                 = "Standard"

  frontend_ip_configuration {
    name                 = "WebUI"
    public_ip_address_id = azurerm_public_ip.web_ui.id
  }
  frontend_ip_configuration {
    name                 = "Swarm"
    public_ip_address_id = azurerm_public_ip.swarm.id
  }

  tags = var.tags
}

# Load Balancer Backend Pool
resource "azurerm_lb_backend_address_pool" "this" {
  name            = var.load_balancer_name
  loadbalancer_id = azurerm_lb.this.id
}

locals {
  lb_ports_map = {
    # frontend_index is 0 for "WebUI", or 1 for "Swarm"
    "https"     = { "port" = "443", "frontend_index" = 0 },
    "websocket" = { "port" = "8084", "frontend_index" = 0 },
    "swarm"     = { "port" = "8080", "frontend_index" = 1 }
  }
}

# Load Balancer Probes
resource "azurerm_lb_probe" "this" {
  for_each        = local.lb_ports_map
  name            = each.key
  protocol        = "Tcp"
  port            = each.value["port"]
  loadbalancer_id = azurerm_lb.this.id
}

# Load Balancer Rule
resource "azurerm_lb_rule" "lb_rule" {
  for_each                       = local.lb_ports_map
  name                           = each.key
  protocol                       = "Tcp"
  frontend_port                  = each.value["port"]
  backend_port                   = each.value["port"]
  frontend_ip_configuration_name = azurerm_lb.this.frontend_ip_configuration[each.value["frontend_index"]].name
  backend_address_pool_ids       = [azurerm_lb_backend_address_pool.this.id]
  probe_id                       = azurerm_lb_probe.this[each.key].id
  loadbalancer_id                = azurerm_lb.this.id
}
