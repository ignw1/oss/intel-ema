# This is probably the most important output, as module.ema_server_vms needs it to associate each of its VMs
# with the Load Balancer (via its azurerm_network_interface_backend_address_pool_association resources)
output "backend_address_pool_id" {
  description = "ID of the load balancer's backend address pool"
  value       = azurerm_lb_backend_address_pool.this.id
}

output "backend_address_pool_name" {
  description = "Name of the load balancer's backend address pool (for associating VMs)"
  value       = azurerm_lb_backend_address_pool.this.name
}

output "web_ui_pip_id" {
  description = "ID of the public IP for Web UI traffic"
  value       = azurerm_public_ip.web_ui.id
}

output "web_ui_pip_address" {
  description = "Public IP address for Web UI traffic"
  value       = azurerm_public_ip.web_ui.ip_address
}

output "swarm_pip_id" {
  description = "ID of the public IP for endpoint PCs/Laptops to connect to the EMA swarm component"
  value       = azurerm_public_ip.swarm.id
}

output "swarm_pip_address" {
  description = "Public IP address for endpoint PCs/Laptops to connect to the EMA swarm component"
  value       = azurerm_public_ip.swarm.ip_address
}

output "lb_id" {
  description = "ID of the load balancer"
  value       = azurerm_lb.this.id
}

output "lb_name" {
  description = "Name of the load balancer"
  value       = azurerm_lb.this.name
}

output "lb_frontend_ip_configuration" {
  description = "Frontend IP configuration(s) of the load balancer"
  value       = azurerm_lb.this.frontend_ip_configuration
}
