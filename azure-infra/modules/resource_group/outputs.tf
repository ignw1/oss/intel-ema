output "id" {
  description = "Azure Resource Group ID"
  value       = azurerm_resource_group.this.id
}

output "name" {
  description = "Azure Resource Group Name"
  value       = azurerm_resource_group.this.name
}
