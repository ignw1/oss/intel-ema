# Azure SQL Server instances are required to have a globally (all of Azure) unique name
resource "random_string" "unique_name_suffix" {
  length  = 16
  upper   = false
  special = false
}

resource "azurerm_mssql_server" "intel_ema" {
  name                         = "${var.sql_server_name_prefix}-${random_string.unique_name_suffix.result}"
  resource_group_name          = var.resource_group_name
  location                     = var.location
  version                      = "12.0"
  administrator_login          = var.sql_admin_username
  administrator_login_password = var.sql_admin_password
  minimum_tls_version          = "1.2"

  /*
  # Uncomment this block to use Azure AD authentication
  azuread_administrator {
    login_username = "AzureAD Admin"
    object_id      = "00000000-0000-0000-0000-000000000000"
  }
  */

  tags = var.tags
}

# Azure SQL Server firewall rule. For more info about expected behaviour, see:
# https://social.msdn.microsoft.com/Forums/en-US/d352afad-68cb-4d80-b9f4-63131a63cbbb/port-1433-open-for-azure-sql-server-even-if-unauthorized?forum=ssdsgetstarted
resource "azurerm_mssql_virtual_network_rule" "allow_ema_servers" {
  name      = "allow-ema-servers"
  server_id = azurerm_mssql_server.intel_ema.id
  subnet_id = var.ema_servers_subnet_id
}
