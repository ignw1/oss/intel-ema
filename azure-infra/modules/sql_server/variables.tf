variable "sql_server_name_prefix" {
  type        = string
  description = "Name prefix for the EMA SQL Server. 16 random chars will be appended to create the full, unique name."
  default     = "intel-ema"
}

variable "sql_admin_username" {
  type        = string
  description = "SQL Server admin username for the EMA server VMs"
  sensitive   = true
}

variable "sql_admin_password" {
  type        = string
  description = "SQL Server admin password for the EMA server VMs"
  sensitive   = true
}

variable "ema_servers_subnet_id" {
  type        = string
  description = "Subnet ID of the EMA servers' subnet"
}

variable "location" {
  type        = string
  description = "Region where the Intel EMA resources will reside"
}

variable "resource_group_name" {
  type        = string
  description = "Name of the Resource Group in which to deploy the Intel EMA resources"
}

variable "tags" {
  type        = map(string)
  description = "Any tags that should added to the EMA resources"
}
