output "ema_sql_server_id" {
  description = "The Azure SQL Server ID of the EMA SQL Server"
  value       = azurerm_mssql_server.intel_ema.id
}

output "ema_sql_server_fqdn" {
  description = "FQDN of the EMA SQL Server"
  value       = azurerm_mssql_server.intel_ema.fully_qualified_domain_name
}

output "ema_sql_server_restorable_dropped_db_ids" {
  description = "A list of dropped restorable database IDs on the EMA SQL Server"
  value       = azurerm_mssql_server.intel_ema.restorable_dropped_database_ids
}