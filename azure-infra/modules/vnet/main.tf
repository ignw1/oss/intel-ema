resource "azurerm_virtual_network" "this" {
  name                = var.vnet_name
  address_space       = var.vnet_address_spaces
  location            = var.location
  resource_group_name = var.resource_group_name
  tags                = var.tags
}
