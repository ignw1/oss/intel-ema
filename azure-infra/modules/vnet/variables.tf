variable "vnet_name" {
  type        = string
  description = "Name of the Virtual Network for Intel EMA resources"
  default     = "intel-ema"
}

variable "vnet_address_spaces" {
  type        = list(string)
  description = "List of address ranges in the Virtual Network"
  default     = ["10.250.0.0/24"]
}

variable "location" {
  type        = string
  description = "Region where the Intel EMA resources will reside"
}

variable "resource_group_name" {
  type        = string
  description = "Name of the Resource Group in which to deploy the Intel EMA resources"
}

variable "tags" {
  type        = map(any)
  description = "Any tags that should added to the EMA resources"
}
