output "id" {
  description = "ID of the Virtual Network"
  value       = azurerm_virtual_network.this.id
}

output "name" {
  description = "Name of the Virtual Network"
  value       = azurerm_virtual_network.this.name
}

output "address_spaces" {
  description = "List of address ranges within the Virtual Network"
  value       = azurerm_virtual_network.this.address_space
}

output "guid" {
  description = "GUID of the Virtual Network"
  value       = azurerm_virtual_network.this.guid
}
