terraform {
  required_version = ">= 1.2.7"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.21.1"
    }
    random = {
      source  = "hashicorp/random"
      version = ">= 3.4.2"
    }
  }
}

provider "azurerm" {
  features {}
}