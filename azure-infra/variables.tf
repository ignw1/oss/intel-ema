variable "location" {
  type        = string
  description = "Region where the Intel EMA resources will reside"
  default     = "East US 2"
}

variable "resource_group_name" {
  type        = string
  description = "Name of the Resource Group in which to deploy the Intel EMA resources"
  default     = "intel-ema-poc"
}

variable "trusted_web_ui_source_addresses" {
  type        = list(string)
  description = "List of address ranges from which trusted EMA users can access the Web UI. Only use [\"0.0.0.0/0\"] for brief testing, and with caution."
}

variable "ema_servers_admin_username" {
  type        = string
  description = "Windows admin username for the EMA server VMs"
  sensitive   = true
}

variable "ema_servers_admin_password" {
  type        = string
  description = "Windows admin password for the EMA server VMs"
  sensitive   = true
}

variable "sql_admin_username" {
  type        = string
  description = "SQL Server admin username for the EMA server VMs"
  sensitive   = true
}

variable "sql_admin_password" {
  type        = string
  description = "SQL Server admin password for the EMA server VMs"
  sensitive   = true
}
