# Output all of the info you'll need during your EMA application installation onto
# this infrastructure.

output "resource_group_name" {
  description = "Name of the resource group"
  value       = module.resource_group.name
}

output "vnet_name" {
  description = "Name of the virtual network"
  value       = module.vnet.name
}

output "ema_servers_vm_names" {
  description = "List of names of the EMA server VMs"
  value       = module.ema_server_vms.ema_servers_vm_names
}

output "ema_servers_nic_private_ips" {
  description = "List of private IP addresses of the EMA servers"
  value       = module.ema_server_vms.ema_servers_nic_private_ips
}

output "web_ui_pip_address" {
  description = "Public IP address of the EMA Web UI"
  value       = module.load_balancer.web_ui_pip_address
}

output "swarm_pip_address" {
  description = "Public IP address of the EMA swarm component"
  value       = module.load_balancer.swarm_pip_address
}

output "ema_sql_server_fqdn" {
  description = "FQDN of the SQL Server instance"
  value       = module.sql_server.ema_sql_server_fqdn
}
